import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  error: JSON;
  model: any = {};
  loading = false;
  returnUrl = '../index-page/index-page.component.html'
  constructor(private httpClient: HttpClient,
              private router: Router
  ) { }

  login() {
        this.loading = true;
        this.check_in_backend(this.model.email, this.model.password)
            .subscribe(

                error => {
                    this.loading = false;
                });

  }
  check_in_backend(email: string, password: string){
        return this.httpClient.post<any>('http://127.0.0.1:5002/login', {email: email, password: password})
            .map(data => {
                console.log(data);
                console.log(data['result'])
                if data['result']=='success'{
                    this.router.navigate(['./index']);
                }
                
                return data;
            });

    }
}

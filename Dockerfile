#########################
### build environment ###
#########################

# base image
FROM node:9.6.1 as builder

# install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable

# set working directory
RUN mkdir /usr/src/loginapp
WORKDIR /usr/src/loginapp


# add `/usr/src/loginapp/node_modules/.bin` to $PATH
ENV PATH /usr/src/loginapp/node_modules/.bin:$PATH

# install and cache app dependencies
COPY /loginapp/package.json /usr/src/loginapp/package.json

#RUN npm install
RUN npm install -g @angular/cli@latest --save


# add app
COPY /loginapp/ /usr/src/loginapp/


# run loginapp
CMD ng serve --host 0.0.0.0

from flask import Flask, request
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify

app = Flask(__name__)
api = Api(app)

CORS(app)
user = {'id':1, 'email':'ktts1993@gmail.com' , 'password':'1234'}

@app.route('/login', methods=['POST'])
def login():
    print("ok")
    error = 'success'
    if request.method == 'POST':
        json_data = request.json
        print(json_data['email'])
        print(json_data['password'])

        if json_data['email'] == user['email'] and json_data['password'] == user['password']:
            print("found")
        else:
            error = 'Invalid Credentials. Please try again.'
    # return (render_template('../../loginapp/app/login-page/login-page.component.html', error=error))
    return jsonify({'result': error})

@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template("../../loginapp/app/index-page/index-page.component.html");


class Users(Resource):
    def get(self):
        return {'users': users}

class Index(Resource):
    def get(self):
        return {users}


api.add_resource(Users, '/users') # Route_3


if __name__ == '__main__':
   app.run(port=5002)
